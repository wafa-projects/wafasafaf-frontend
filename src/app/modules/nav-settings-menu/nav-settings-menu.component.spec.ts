import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavSettingsMenuComponent } from './nav-settings-menu.component';

describe('NavSettingsMenuComponent', () => {
  let component: NavSettingsMenuComponent;
  let fixture: ComponentFixture<NavSettingsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavSettingsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavSettingsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
