import { Component, OnInit, Input } from '@angular/core';
import { AccountsService } from 'src/app/shared/providers/accounts.service';
import { AuthProvider } from 'src/app/shared/providers/auth.service';

@Component({
  selector: 'app-nav-settings-menu',
  templateUrl: './nav-settings-menu.component.html',
  styleUrls: ['./nav-settings-menu.component.css']
})
export class NavSettingsMenuComponent implements OnInit {
 @Input()
  userConnected
  constructor(private accountService:AuthProvider) { }

  ngOnInit(): void {
  }

  logout() {
    this.accountService.signOut()
  }

}
