import { NgModule, Host } from '@angular/core';
import { CommonModule } from '@angular/common';
 
import { RouterModule, Routes } from '@angular/router';
 
import { NavSettingsMenuComponent } from './nav-settings-menu/nav-settings-menu.component';
import { FooterComponent } from './footer/footer.component';
 

import { NavbarComponent } from './navbar/navbar.component';
import { LoggedModuleModule } from './logged-module/logged-module.module';
import { RootComponent } from './root/root.component';

import { PublicModuleModule } from './public-module/public-module.module';
import { SharedModule } from '../shared/shared.module';
 



@NgModule({
  declarations: [

    RootComponent,
    NavbarComponent,
    NavSettingsMenuComponent, 
    FooterComponent, 
     RootComponent,

  ],
  imports: [
    CommonModule,
    RouterModule,
    PublicModuleModule,
    LoggedModuleModule,
    SharedModule
 
  ],
  exports :[
    RouterModule,
    RootComponent,
    NavSettingsMenuComponent, 
    NavbarComponent,
    FooterComponent, 
    
  
  ],
   providers: [],
  
  
})
export class ModulesModule { }
