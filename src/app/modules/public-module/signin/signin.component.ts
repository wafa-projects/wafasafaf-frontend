import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AccountsService } from 'src/app/shared/providers/accounts.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-digi-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {


  registerForm: FormGroup;
  submitted = false;
 
  
  constructor(private formBuilder: FormBuilder,private router:Router,private authentficationService:AccountsService,private toaster: ToastrService) {
   
	  this.registerForm = this.formBuilder.group({
 
            email: [null, { validators: [Validators.required, Validators.email] }],
            password : [null, Validators.required],
        
    } );
  }


  ngOnInit() {
 
  }

  
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      debugger
 
      if (this.registerForm.invalid) {
          return;
      }

      this.authentficationService.login(this.registerForm.value).subscribe(res => {
	 
        localStorage.setItem(environment.TOKEN_NAME, JSON.stringify(res));
        this.router.navigate(['/mon-espace/articles']);
   
      },err=>{
        this.toaster.error(err.error.message);
      });

  }
  

}
