import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AccountsService } from 'src/app/shared/providers/accounts.service';

import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


const   text ="[^0-9]*";
@Component({
  selector: 'app-digi-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  @ViewChild('recaptcha', {static: true }) recaptchaElement: ElementRef;
 
  registerForm: FormGroup;
  submitted = false;
 
  
  constructor(private formBuilder: FormBuilder,private authentficationService:AccountsService,private toaster: ToastrService) {
   
	  this.registerForm = this.formBuilder.group({

            nom: [null,{ validators: [Validators.required, Validators.pattern(text)] }],
            prenom: [null,{ validators: [Validators.required, Validators.pattern(text)] }],
            matricule: [null,{ validators: [Validators.required] }],
            ville: [null,{ validators: [Validators.required, Validators.pattern(text)] }],
            region: [null,{ validators: [Validators.required, Validators.pattern(text)] }],
            entite: [null,{ validators: [Validators.required] }],
            email: [null, { validators: [Validators.required, Validators.email] }],
            password : [null, Validators.required],
            repeatpassword : [null, Validators.required],
            accept_terms : [null, Validators.required],
        
    },{
      validator: this.MustMatchPassword('password', 'repeatpassword')
    });
  }

  MustMatchPassword(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
           
            return;
        }
 
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}



  ngOnInit(): void {
    this.addRecaptchaScript();
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      debugger

      if (this.registerForm.invalid) {
          return;
      }

      this.authentficationService.signup(this.registerForm.value).subscribe(res => {
  
        localStorage.setItem(environment.TOKEN_NAME, atob(JSON.stringify(res)));
        this.toaster.success('success', 'Votre inscription a été effectuer avec success.Veuillez verifier votre email');
        
  
      },err=>{
        this.toaster.error(err.error.message);
      });

  }
      
  renderReCaptch() {
    window['grecaptcha'].render(this.recaptchaElement.nativeElement, {
      'sitekey' : '6LePbq4UAAAAAPqwJU8u5g1Of1TIEMyoPpJQpyaD',
      'callback': (response) => {
        debugger
          console.log(response);
      }
    });
  }
 
  addRecaptchaScript() {
 
    window['grecaptchaCallback'] = () => {
      this.renderReCaptch();
    }
 
    (function(d, s, id, obj){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { obj.renderReCaptch(); return;}
      js = d.createElement(s); js.id = id;
      js.src = "https://www.google.com/recaptcha/api.js?onload=grecaptchaCallback&amp;render=explicit";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'recaptcha-jssdk', this));
 
  }

}
