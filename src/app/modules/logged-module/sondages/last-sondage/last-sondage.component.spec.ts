import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastSondageComponent } from './last-sondage.component';

describe('LastSondageComponent', () => {
  let component: LastSondageComponent;
  let fixture: ComponentFixture<LastSondageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastSondageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastSondageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
