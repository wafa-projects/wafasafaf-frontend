import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 
import { SharedModule } from 'src/app/shared/shared.module';
import { SondageDetailQuestionsComponent } from './sondage-detail-questions/sondage-detail-questions.component';
import { LastSondageComponent } from './last-sondage/last-sondage.component';
import { SondageDetailComponent } from './sondage-detail/sondage-detail.component';
import { SondagesListeComponent } from './sondages-liste/sondages-liste.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    SondageDetailQuestionsComponent,
    SondageDetailComponent,
    LastSondageComponent,
    SondagesListeComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  exports: [
    SondageDetailQuestionsComponent,
    SondageDetailComponent,
    LastSondageComponent,
    SondagesListeComponent
  ]
})
export class SondagesModule { }
