import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/shared/providers/pageSharing';

@Component({
  selector: 'app-sondages-liste',
  templateUrl: './sondages-liste.component.html',
  styleUrls: ['./sondages-liste.component.css']
})
export class SondagesListeComponent implements OnInit {

  sondages = [
    {
      id:"11",
      title : "Solution digitale ",
      description : "Nous vous offrons à nos prospects une solution innovante qui leur permettra d'effectuer leur demande de crédit en ligne. Partagez avec nous votre avis ",
      datePublication: "02/17/2020 17:20:30",
    },

    {
      id:"12",
      title : "Agence  ",
      description : "Vous vous êtes rendu dernièrement dans l'une de nos agences ou vous travaillez dans une agence, partagez avec nous votre avis sur l'expérience client. ",
      datePublication: "08/14/2010 17:20:30",
    }


  ]

  activeComponenet
  constructor(private pageCurrentService: DataService) { 

  }

  ngOnInit(): void {
    this.pageCurrentService.currentPage.subscribe(message => this.activeComponenet = message)
  }

  changeCurrentPage(page): void {
    this.pageCurrentService.changeCurrentPage(page)
  } 

}
