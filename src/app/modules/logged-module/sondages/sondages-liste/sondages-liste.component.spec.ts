import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SondagesListeComponent } from './sondages-liste.component';

describe('SondagesListeComponent', () => {
  let component: SondagesListeComponent;
  let fixture: ComponentFixture<SondagesListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SondagesListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SondagesListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
