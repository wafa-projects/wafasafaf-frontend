import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SondageDetailQuestionsComponent } from './sondage-detail-questions.component';

describe('SondageDetailQuestionsComponent', () => {
  let component: SondageDetailQuestionsComponent;
  let fixture: ComponentFixture<SondageDetailQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SondageDetailQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SondageDetailQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
