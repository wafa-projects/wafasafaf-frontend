import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sondage-detail-questions',
  templateUrl: './sondage-detail-questions.component.html',
  styleUrls: ['./sondage-detail-questions.component.css']
})
export class SondageDetailQuestionsComponent implements OnInit {


  @Input()
  question
  constructor() { }

  ngOnInit(): void {
  }

}
