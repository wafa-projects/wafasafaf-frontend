import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Router } from '@angular/router';
import { ArticlesProvider } from 'src/app/shared/providers/articles';
import { DataService } from 'src/app/shared/providers/pageSharing';

@Component({
  selector: 'app-articles-add',
  templateUrl: './articles-add.component.html',
  styleUrls: ['./articles-add.component.css']
})
export class ArticlesAddComponent implements OnInit {

  activeComponenet
  addForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private apiService: ArticlesProvider,private pageCurrentService:DataService) { }

  modules = {
 
    toolbar: [      
      [{ header: [1, 2, false] }],
      [{ 'color': [] }, { 'background': [] }], 
      [{ 'align': [] }],
      ['bold', 'italic', 'underline'],
  
      ['blockquote', 'code-block','link', 'image', 'video'],
     
      ['clean'],  
    ],
  
  };

  
  
  ngOnInit() {
    this.pageCurrentService.currentPage.subscribe(message => this.activeComponenet = message)
    this.addForm = this.formBuilder.group({
     
      titre: ['', ],
      description: ['', ],
      utilisateur: ['Me',],
    });

  }


  onSubmit() {
    debugger
    this.apiService.postArticle(this.addForm.value)
      .subscribe( data => {
        this.router.navigate(['articles']);
      });
  }

  
 

  changeCurrentPage(page): void {
    this.pageCurrentService.changeCurrentPage(page)
  }


}
