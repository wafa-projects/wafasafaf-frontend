import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticlesListeComponent } from './articles-liste/articles-liste.component';
import { ArticlesAddComponent } from './articles-add/articles-add.component';
import { ArticleCommentsComponent } from './article-comments/article-comments.component';
import { QuillModule } from 'ngx-quill';
 
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 

@NgModule({
  declarations: [
    ArticlesListeComponent,
    ArticlesAddComponent,
    ArticleCommentsComponent,
    
    
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    QuillModule.forRoot(),
  ],
  exports: [
    ArticlesListeComponent,
    ArticlesAddComponent,
    ArticleCommentsComponent
  ],
})
export class ArticlesModule { }
