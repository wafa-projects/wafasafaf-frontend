import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/shared/providers/pageSharing';

@Component({
  selector: 'app-articles-liste',
  templateUrl: './articles-liste.component.html',
  styleUrls: ['./articles-liste.component.css']
})
export class ArticlesListeComponent implements OnInit {
  loggedUser = 'Hassan'
  articles = [

    {
      id:'5214',
 
      dateCreation:'1/12/2020 15:20:30',
      title:'Faire ses courses',
      body:'<b> Pour gagner du temps (et de l’argent !) </b>, faire ses courses est une question d’organisation. Liste de courses avant de partir, achats dans le magasin et rangements après en rentrant chez soi : voici nos astuces.',
      countComments : 15,
      countLikes : 15,
      countVues : 135,
      likedBy:true,
      sliceDescription: 120,
      commentsShowd : false,
      user : {
        profilePicture : "https://secure.gravatar.com/avatar/1bab1414d8b4d5afda9a852238053f37?s=800&d=identicon",
        name : "Ayoub BENABDELAZIZ",
      },
      comments : [
        {
          user : {
            profilePicture : "https://gitlab.com/uploads/-/system/user/avatar/892131/avatar.png?width=400",
            name : "Hassan ABOUDRAR",
          },
          comment : "Ceci est un commentaire",
          dateComment:'2/18/2020 13:20:30'
        },
        {
          user : {
            profilePicture : "https://gitlab.com/uploads/-/system/user/avatar/892131/avatar.png?width=400",
            name : "Hassan ABOUDRAR",
          },
          comment : "Ceci est un commentaire",
          dateComment:'2/18/2020 17:20:30'
        },
        
        {


          user : {
            profilePicture : "https://gitlab.com/uploads/-/system/user/avatar/892131/avatar.png?width=400",
            name : "Ayoub BENABDELAZIZ",
          },
          comment : "Merci pour votre commentaire",
   
          dateComment:'2/19/2020 17:20:30'
        }

      ]
    },
    {
      id:'123456',
    
      dateCreation:'2/19/2020 15:20:30',
      title:"Economiser de l'argent au quotidien : 9 conseils",
      body:'Vous avez du mal à économiser de l’argent ? Vous aimeriez financer vos projets ? Découvrez nos 9 meilleurs conseils pour économiser aujourd’hui et pas demain ! ',
      countComments : 15,
      countLikes : 15,
      countVues : 15,
      likedBy:true,
      sliceDescription: 120,
      commentsShowd : false,
      user : {
        profilePicture : "https://gitlab.com/uploads/-/system/user/avatar/892131/avatar.png?width=400",
        name : "Hassan ABOUDRAR",
      },
      comments : [
        {
          user : {
            profilePicture : "https://gitlab.com/uploads/-/system/user/avatar/892131/avatar.png?width=400",
            name : "Ayoub BENABDELAZIZ",
          },
          comment : "Ceci est un commentaire",
          dateComment:'2/18/2020 17:20:30'
        },
        {
          user : {
            profilePicture : "https://gitlab.com/uploads/-/system/user/avatar/892131/avatar.png?width=400",
            name : "Hassan ABOUDRAR",
          },
          comment : "Merci pour votre commentaire",
          dateComment:'2/18/2020 17:20:30'
        }

      ]
      
    },
    {
      id:'654321',
  
      dateCreation:'2/17/2020 17:20:30',
      title:'Budget familiale : nos conseils & astuces',
      body:"Faire ses comptes familiaux est une activité qui peut rapidement devenir compliquée ! Elle implique fréquemment de devoir « jongler » entre différents comptes en banques : son compte personnel, celui de son conjoint ainsi que les comptes communs... La liste s'allonge très vite ! Nous pouvons ajouter à cela les éventuels crédits et comptes épargnes pour comprendre que la situation peut devenir rapidement ingérable sans un minimum d'organisation.Suivre son budget familial est pourtant un excellent moyen de gagner en sérénité au quotidien. C'est aussi une bonne solution pour se donner les moyens de réaliser ses grands projets familiaux : départ en vacances, nouveau logement Ainsi, nous vous proposons ici quelques conseils pour démarrer du bon pied dans votre gestion familiale.",
      countComments : 19,
      countLikes : 10,
      countVues : 155,
      likedBy:false,
      sliceDescription: 120,
      commentsShowd : false,
      user : {
        profilePicture : "https://gitlab.com/uploads/-/system/user/avatar/892131/avatar.png?width=400",
        name : "Hassan ABOUDRAR",
      },
      comments : [
        {
          user : {
            profilePicture : "https://gitlab.com/uploads/-/system/user/avatar/892131/avatar.png?width=400",
            name : "Ayoub BENABDELAZIZ",
          },
          comment : "Ceci est un commentaire",
          dateComment:'2/18/2020 17:20:30'
        },
        {
          user : {
            profilePicture : "https://gitlab.com/uploads/-/system/user/avatar/892131/avatar.png?width=400",
            name : "Hassan ABOUDRAR",
          },
          comment : "Merci pour votre commentaire",
          dateComment:'2/18/2020 17:20:30'
        }

      ]
    },

  ]

  activeComponenet
  constructor(private pageCurrentService: DataService) { 

  }

  ngOnInit(): void {
    this.pageCurrentService.currentPage.subscribe(message => this.activeComponenet = message)
  }

  changeCurrentPage(page): void {
    this.pageCurrentService.changeCurrentPage(page)
  }
 
 

}
