import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesListeComponent } from './articles-liste.component';

describe('ArticlesListeComponent', () => {
  let component: ArticlesListeComponent;
  let fixture: ComponentFixture<ArticlesListeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlesListeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
