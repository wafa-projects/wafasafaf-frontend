import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountsComponent } from './accounts/accounts.component';
import { IdeasComponent } from './ideas/ideas.component';
import { SondagesComponent } from './sondages/sondages.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { AdminPageComponent } from './pages/admin-page/admin-page.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { AdminSideBarComponent } from './admin-side-bar/admin-side-bar.component';



@NgModule({
  declarations: [
 
    AccountsComponent, 
    IdeasComponent, 
    SondagesComponent, 
    SideBarComponent, 
    AdminPageComponent, AdminSideBarComponent],
  imports: [
    CommonModule,

    RouterModule,
    BrowserModule
 
  ],
  exports :[

    AccountsComponent, 
    IdeasComponent, 
    SondagesComponent, 
    SideBarComponent, 
    AdminPageComponent
    
  
  ],
   providers: [],
})
export class AdminModule { }
