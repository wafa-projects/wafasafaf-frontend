import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-side-bar',
  templateUrl: './admin-side-bar.component.html',
  styleUrls: ['./admin-side-bar.component.css']
})
export class AdminSideBarComponent implements OnInit {

  sideBarItems = [
    {
        title : 'Utilisateurs',
        icon: 'la la-lightbulb-o',
        link: '/admininstration/utilisateurs'
    },
    {
      title : 'Idées',
      icon: 'la la-question-circle ',
      link: '/admininstration/idees'
  },
  {
    title : 'Articles',
    icon: 'la la-files-o ',
    link: '/admininstration/articles'
    
},
{
  title : "Enquêtes",
  icon: 'la la-area-chart  ',
  link: '/admininstration/acceuil'
  
},
{
  title : "Page d'acceuil",
  icon: 'la la-area-chart  ',
  link: '/admininstration/acceuil'
  
},


];
  constructor() { }

  ngOnInit(): void {
  }

}
