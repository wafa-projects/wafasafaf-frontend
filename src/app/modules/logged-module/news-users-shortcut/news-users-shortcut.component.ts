import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-users-shortcut',
  templateUrl: './news-users-shortcut.component.html',
  styleUrls: ['./news-users-shortcut.component.css']
})
export class NewsUsersShortcutComponent implements OnInit {

  users=[
    {
      profilePicture : "https://secure.gravatar.com/avatar/1bab1414d8b4d5afda9a852238053f37?s=800&d=identicon",
      name : "Ayoub BENABDELAZIZ",
      fonction : "Ingenieur FULLSTACK ",
    
    },{
 
      profilePicture : "https://gitlab.com/uploads/-/system/user/avatar/892131/avatar.png?width=400",
      name : "Hassan ABOUDRAR",
      fonction : "Ingenieur FULLSTACK ",
    } 
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
