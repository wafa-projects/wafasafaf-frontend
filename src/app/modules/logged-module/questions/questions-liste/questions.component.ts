import { Component, OnInit } from '@angular/core';
import { AuthProvider } from 'src/app/shared/providers/auth.service';
import { QuestionProvider } from 'src/app/shared/providers/questions';

@Component({
  selector: 'app-questions-liste',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {

  questions = [
    

  ]
  constructor(private authService:AuthProvider,private questionProvider:QuestionProvider) { }

  ngOnInit(): void {
    this.loadQuestionByUser()
  }

  loadQuestionByUser() {
 
    let userLogged = JSON.parse(this.authService.retrieveLoggedUser());
    
    this.questionProvider.getQuestionByUser(userLogged.utilisateurId,{})
      .subscribe( data => {
         this.questions = data['content']
      },err=>{
        //this.toaster.error('danger', err.error.message);
      });

}

}
