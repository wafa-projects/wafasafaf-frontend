import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MiddleContainerProfileComponent } from './middle-container-profile.component';

describe('MiddleContainerProfileComponent', () => {
  let component: MiddleContainerProfileComponent;
  let fixture: ComponentFixture<MiddleContainerProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiddleContainerProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiddleContainerProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
