import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SondagesPageComponent } from './sondages-page.component';

describe('SondagesPageComponent', () => {
  let component: SondagesPageComponent;
  let fixture: ComponentFixture<SondagesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SondagesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SondagesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
