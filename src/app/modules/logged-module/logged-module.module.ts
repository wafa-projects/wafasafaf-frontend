import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
 
import { MiddleContainerProfileComponent } from './middle-container-profile/middle-container-profile.component';
import { SideRightBarProfileComponent } from './side-right-bar-profile/side-right-bar-profile.component';
 
 
 
import { NewsUsersShortcutComponent } from './news-users-shortcut/news-users-shortcut.component';
 
import { LoggedPageComponent } from './logged-page/logged-page.component';
import { SideBarProfileComponent } from './side-bar-profile/side-bar-profile.component';
 

import { ProfileInfoComponent } from './profile/profile-info/profile-info.component';
import { ArticlesModule } from './articles/articles.module';
import { SondagesModule } from './sondages/sondages.module';
import { QuestionsModule } from './questions/questions.module';
import { IdeasModule } from './ideas/ideas.module';

import { RouterModule,Routes } from '@angular/router';
 
import { SharedModule } from 'src/app/shared/shared.module';
import { ArticlesPageComponent } from './pages/articles-page/articles-page.component';
import { IdeasPageComponent } from './pages/ideas-page/ideas-page.component';
import { SondagesPageComponent } from './pages/sondages-page/sondages-page.component';
import { QuestionsPageComponent } from './pages/questions-page/questions-page.component';
import { GroupeAddBtnComponent } from './groupe-add-btn/groupe-add-btn.component';
import { AdminPageComponent } from './admin/pages/admin-page/admin-page.component';
import { AdminModule } from './admin/admin.module';
 
 


@NgModule({
  declarations: [

    
    LoggedPageComponent, 
    SideBarProfileComponent, 
    MiddleContainerProfileComponent, 
    SideRightBarProfileComponent, 
  
    ProfileInfoComponent, 
    NewsUsersShortcutComponent, 
    
    ArticlesPageComponent,
    IdeasPageComponent, 
    SondagesPageComponent, 
    QuestionsPageComponent, 
    GroupeAddBtnComponent, 
  
  ],
  exports: [
    LoggedPageComponent, 
    SideBarProfileComponent, 
    MiddleContainerProfileComponent, 
    SideRightBarProfileComponent, 
    ProfileInfoComponent, 
    NewsUsersShortcutComponent, 
    RouterModule,
    ArticlesPageComponent,
    IdeasPageComponent, 
    SondagesPageComponent, 
    QuestionsPageComponent, 
    AdminModule,
    GroupeAddBtnComponent
  
  ],
  imports: [
    CommonModule,
    ArticlesModule,
    SondagesModule,
    QuestionsModule,
    IdeasModule,
    SharedModule,
    AdminModule,
    RouterModule
  ]
})
export class LoggedModuleModule { }
