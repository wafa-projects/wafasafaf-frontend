import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupeAddBtnComponent } from './groupe-add-btn.component';

describe('GroupeAddBtnComponent', () => {
  let component: GroupeAddBtnComponent;
  let fixture: ComponentFixture<GroupeAddBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupeAddBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupeAddBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
