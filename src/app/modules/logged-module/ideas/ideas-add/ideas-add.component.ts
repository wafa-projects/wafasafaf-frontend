import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ArticlesProvider } from 'src/app/shared/providers/articles';
import { DataService } from 'src/app/shared/providers/pageSharing';

@Component({
  selector: 'app-ideas-add',
  templateUrl: './ideas-add.component.html',
  styleUrls: ['./ideas-add.component.css']
})
export class IdeasAddComponent implements OnInit {
  activeComponenet
  addForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private apiService: ArticlesProvider,private pageCurrentService:DataService) { }
 
  files: any = [];

  uploadFile(event) {
    for (let index = 0; index < event.length; index++) {
      
      const element = event[index];
      this.files.push(element.name)
    }  
  }
  deleteAttachment(index) {
    this.files.splice(index, 1)
  }
  
  
  ngOnInit() {
    this.pageCurrentService.currentPage.subscribe(message => this.activeComponenet = message)
    this.addForm = this.formBuilder.group({
     
      titre: ['', ],
    
      body: ['Me',],
    });

  }


  onSubmit() {
    debugger
    this.apiService.postArticle(this.addForm.value)
      .subscribe( data => {
        this.router.navigate(['articles']);
      });
  }

  
 

  changeCurrentPage(page): void {
    this.pageCurrentService.changeCurrentPage(page)
  }


  modules = {
   
    toolbar: [      
      [{ header: [1, 2, false] }],
      ['bold', 'italic', 'underline'],

      ['clean'],  
    ]
  };
 

}
