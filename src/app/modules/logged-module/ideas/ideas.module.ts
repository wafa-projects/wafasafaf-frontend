import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IdeasComponent } from './ideas-liste/ideas.component';
import { IdeasAddComponent } from './ideas-add/ideas-add.component';
import { QuillModule } from 'ngx-quill';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 

@NgModule({
  declarations: [
    IdeasComponent,
    IdeasAddComponent,
     
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    QuillModule.forRoot(),
  ],
  exports: [
    IdeasComponent,
    IdeasAddComponent
  ],
})
export class IdeasModule { }
