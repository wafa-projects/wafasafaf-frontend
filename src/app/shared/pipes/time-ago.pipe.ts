 

import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'timeAgo',
  pure: true
})
export class TimeAgoPipe implements PipeTransform {
    transform(value: any, args?: any): any {
        if (value) {
            const seconds = Math.floor((+new Date() - +new Date(value)) / 1000);
            if (seconds < 29) // less than 30 seconds ago will show as 'Just now'
                return 'Maintenent';
            const intervals = {
                'anness': 31536000,
                'mois': 2592000,
                'semaines': 604800,
                'jour': 86400,
                'heur': 3600,
                'minute': 60,
                'second': 1
            };
            let counter;
            for (const i in intervals) {
                counter = Math.floor(seconds / intervals[i]);
                if (counter > 0)
                    if (counter === 1) {
                        return "Il y a " + counter + ' ' + i + ' '; // singular (1 day ago)
                    } else {
                        return "Il y a " +counter + ' ' + i + 's '; // plural (2 days ago)
                    }
            }
        }
        return value;
    }

}