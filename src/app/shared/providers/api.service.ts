import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
 

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'multipart/form-data'
  })
};

@Injectable()
export class ApiProvider {


  baseUrl = environment.url +"/api";
 
  constructor(
    private readonly http: HttpClient,
  ) { }

  login(body: any) {
    return this.http.post(this.baseUrl + '/utilisateurs/auth', body);
  }


  signup(body: any) {
    return this.http.post(this.baseUrl + '/utilisateurs', body);
  }

  getAllArticles() {
    return this.http.get(this.baseUrl + '/articles');
  }

  postArticle(body: any) {
    return this.http.post(this.baseUrl + '/articles', body);
  }

  updateArticle(body: any) {
    return this.http.put(this.baseUrl + '/articles', body);
  }

  
  getAllQuestions() {
    return this.http.get(this.baseUrl + '/question');
  }

  postQuestion(body: any) {
    return this.http.post(this.baseUrl + '/question', body);
  }

  getQuestionByUser(id:any,criteria:any) {
    return this.http.get(this.baseUrl + '/question/user/'+id);
  }


  getComments(articleId) {
    return this.http.get(this.baseUrl + '/articles/'+articleId+'/comments');
  }

  postComment(body: any) {
    return this.http.post(this.baseUrl + '/commentaires', body);
  }


  getIdeas() {
    return this.http.get(this.baseUrl + '/idees/');
  }

  getIdeesByUser(userId) {
    return this.http.get(this.baseUrl + '/utilisateur/'+userId+'/idees');
  }
  



}
