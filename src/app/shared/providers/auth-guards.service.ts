import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthProvider } from './auth.service';
 

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthProvider, public router: Router) {}
  canActivate(): boolean {
    if (!this.auth.isLoggedIn()) {
      this.router.navigate(['/utilisateur/authentification']);
      return false;
    }
    return true;
  }
}