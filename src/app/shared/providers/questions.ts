 
import { Injectable } from '@angular/core';
 
 
import { ApiProvider } from './api.service';

@Injectable()
export class QuestionProvider {

  constructor(
    private readonly api: ApiProvider
  ) {}

  loadQuestions() {
 
  }

  postQuestion(question) {
    return this.api.postQuestion(question);
  }

  
  getQuestionByUser(userId,criteria) {
    return this.api.getQuestionByUser(userId,criteria)
  }

}
