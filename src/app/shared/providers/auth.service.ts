import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
 


@Injectable()
export class AuthProvider {

  private loggedIn: boolean;

  constructor(
    private readonly router: Router
  ) {
    this.checkAuthentication();
  }

  isLoggedIn(): boolean {
    return this.loggedIn;
  }

  signIn(token: string): void {
    localStorage.setItem(environment.TOKEN_NAME, token);
    this.loggedIn = true;
  }

  signOut(): void {
    localStorage.removeItem(environment.TOKEN_NAME);
    this.loggedIn = false;
    this.router.navigate(['/utilisateur/authentification']);
  }

  checkAuthentication(): void {
    const token: string = localStorage.getItem(environment.TOKEN_NAME);
    token ? this.signIn(token) : this.signOut();
  }

  retrieveLoggedUser() {
    const user: string =  localStorage.getItem(environment.TOKEN_NAME);
    return user;
  }

}
