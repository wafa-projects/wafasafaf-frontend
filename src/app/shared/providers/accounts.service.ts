import { Injectable } from '@angular/core';
import { ApiProvider } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(
    private readonly api: ApiProvider
  ) {}

   login(login) {
    return this.api.login(login)
  }
  signup(login) {
    return this.api.signup(login)
  }
 
 
}
