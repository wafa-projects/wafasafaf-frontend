 
import { Injectable } from '@angular/core';
 
 
import { ApiProvider } from './api.service';

@Injectable()
export class ArticlesProvider {

  constructor(
    private readonly api: ApiProvider
  ) {}

  loadArticles() {
 
  }

  postArticle(article) {
    return this.api.postArticle(article);
  }

}
