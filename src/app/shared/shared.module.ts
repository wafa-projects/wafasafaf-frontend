import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeAgoPipe } from './pipes/time-ago.pipe';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ArticlesProvider } from './providers/articles';
import { ApiProvider } from './providers/api.service';
import { DataService } from './providers/pageSharing';
import { QuestionProvider } from './providers/questions';
import { UploadDirective } from './directives/upload.directive';
import { InterceptorProvider } from './interceptors/http-interceptor';
import { AuthProvider } from './providers/auth.service';
import { AccountsService } from './providers/accounts.service';
import { RouterModule } from '@angular/router';
import { AuthGuardService } from './providers/auth-guards.service';


@NgModule({
  declarations: [TimeAgoPipe, UploadDirective],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    HttpClientModule,
  
  ],
  providers:[ArticlesProvider,ApiProvider,DataService,QuestionProvider,InterceptorProvider,AuthProvider,AccountsService,AuthGuardService],
  exports:[TimeAgoPipe]
})
export class SharedModule { }
